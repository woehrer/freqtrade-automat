import time
import logging
import sys

import helper.functions
import helper.config
import helper.sql
import helper.telegramsend

#Inizialisierung der Konfiguration
conf = helper.config.initconfig()

helper.functions.initlogger()

# First check is all sucessful
helper.functions.checkinstallfreqtrade()
helper.functions.checkcreateuserdirectory()
helper.functions.checkcreateconfig()

logging.info("Automat Start")

# Download Freqtrade
if conf['downloadedfreqtrade'] == 'False':
    helper.functions.installfreqtrade()
    
# Create User Directory
if conf['createduserdirectory'] == 'False' and conf['downloadedfreqtrade'] == 'True':
    helper.functions.createuserdirectory()

# Download Strategies
if conf['downloadedstrategies'] == 'False' and conf['createduserdirectory'] == 'True':
    helper.functions.downloadstrategies()

# Create Config
if conf['createconfig'] == 'False' and conf['downloadedstrategies'] == 'True':
    helper.functions.createconfig()

# Create SQL Database
helper.sql.create()

if len(sys.argv) > 1:
    if sys.argv[1] == 'hyperopting':
        # Downloading Test Data and Hyperopting
        # TODO Check for only 1 time starting hyperopting
        if conf['hyperopting'] == 'False' and conf['createconfig'] == 'True':
            helper.config.sethyperopting()
            helper.functions.downloadtestdata()
            helper.functions.hyperopting(conf['strategy'], conf['hyperopt_loss'], conf['hyperoptepochs'], conf['hyperopt_timeframe'], conf['hyperopt_timerange_days'])
            helper.config.resethyperopting()
        else:
            helper.telegramsend.send("Hyperopting is already running")
            logging.error("Hyperopting already started")

    if sys.argv[1] == 'start':
        # Start Freqtrade
        if conf['setfreqtradebot'] == 'True':
            helper.functions.start_freqtrade()
            helper.config.setfreqtradebot()
        else:
            helper.telegramsend.send("Freqtrade is already running")
            logging.error("Freqtrade already started")

    if sys.argv[1] == 'stop':
        # Stop Freqtrade
        if conf['setfreqtradebot'] == 'False':
            helper.functions.stop_freqtrade()
            helper.config.resetfreqtradebot()
        else:
            helper.telegramsend.send("Freqtrade is not running")
            logging.error("Freqtrade is not running")
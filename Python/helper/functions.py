import logging
import os
import datetime
import json

import helper.config
import helper.sql
import helper.telegramsend

def initlogger():
	if not os.path.isdir("./logs"):
		os.mkdir("./logs")
	logger = logging.getLogger()
	logger.setLevel(logging.DEBUG)
	handler = logging.FileHandler("logs/automat.log")
	formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
	handler.setFormatter(formatter)
	handler.setLevel(logging.DEBUG)
	logger.addHandler(handler)

def installfreqtrade():
	logging.info("installing freqtrade")
	#Create Dir
	if not os.path.isdir("./freqtrade"):
		os.mkdir("./freqtrade")
		logging.info("Created freqtrade directory")
	#Download Freqtrade
	logging.info("Downloading freqtrade")
	os.system("curl https://raw.githubusercontent.com/freqtrade/freqtrade/stable/docker-compose.yml -o ./freqtrade/docker-compose.yml")
	
	# Check it was sucessful
	checkinstallfreqtrade()

def checkinstallfreqtrade():
	if os.path.isfile("./freqtrade/docker-compose.yml"):
		helper.config.setdownloadedfreqtrade()
		logging.info("freqtrade is installed")
	else:
		helper.config.resetdownloadedfreqtrade()
		logging.error("freqtrade is not installed")


def createuserdirectory():
	logging.info("Create User Directory")
	os.system("/usr/bin/docker-compose -f ./freqtrade/docker-compose.yml run --rm freqtrade create-userdir --userdir user_data")
	logging.info("Created User Directory")
	# Check it was sucessful
	checkcreateuserdirectory()

def checkcreateuserdirectory():
	if os.path.isdir("./freqtrade/user_data"):
		helper.config.setcreateduserdirectory()
		logging.info("User Directory in created")
	else:
		helper.config.resetcreateduserdirectory()
		logging.error("User Directory not created")

def downloadstrategies():
	logging.info("Download Strategies")
	os.system("curl -L https://github.com/freqtrade/freqtrade-strategies/archive/refs/heads/master.zip -o ./freqtrade/user_data/strategies/freqtrade-strategies.zip")
	os.system("unzip -o ./freqtrade/user_data/strategies/freqtrade-strategies.zip -d ./freqtrade/user_data/strategies/")
	os.system("rm ./freqtrade/user_data/strategies/freqtrade-strategies.zip")
	os.system("cp -a ./freqtrade/user_data/strategies/freqtrade-strategies-master/user_data/strategies/. ./freqtrade/user_data/strategies/")
	os.system("cp -a ./freqtrade/user_data/strategies/freqtrade-strategies-master/user_data/hyperopts/. ./freqtrade/user_data/hyperopts/")
	os.system("rm -rf ./freqtrade/user_data/strategies/freqtrade-strategies-master")
	os.system("cp -a ./freqtrade/user_data/strategies/berlinguyinca/. ./freqtrade/user_data/strategies/")
	os.system("rm -rf ./freqtrade/user_data/strategies/berlinguyinca")
	os.system("cp -a ./freqtrade/user_data/strategies/futures/. ./freqtrade/user_data/strategies/")
	os.system("rm -rf ./freqtrade/user_data/strategies/futures")
	os.system("cp -a ./freqtrade/user_data/strategies/lookahead_bias/. ./freqtrade/user_data/strategies/")
	os.system("rm -rf ./freqtrade/user_data/strategies/lookahead_bias")

	os.system("curl -L https://github.com/paulcpk/freqtrade-strategies-that-work/archive/refs/heads/main.zip -o ./freqtrade/user_data/strategies/freqtrade-strategies-that-work.zip")
	os.system("unzip -o ./freqtrade/user_data/strategies/freqtrade-strategies-that-work.zip -d ./freqtrade/user_data/strategies/")
	os.system("rm ./freqtrade/user_data/strategies/freqtrade-strategies-that-work.zip")
	os.system("cp -a ./freqtrade/user_data/strategies/freqtrade-strategies-that-work-main/. ./freqtrade/user_data/strategies/")
	os.system("rm -rf ./freqtrade/user_data/strategies/freqtrade-strategies-that-work-main")
	logging.info("Finish Download Strategies")

	helper.config.setdownloadedstrategies()
	# TODO Check it was sucessful

def createconfig():
	logging.info("Copy Config")
	os.system("cp ./example/config.json ./freqtrade/user_data/")
	logging.info("Finish Copy Config")

	# Check it was sucessful
	checkcreateconfig()

def checkcreateconfig():
	if os.path.isfile("./freqtrade/user_data/config.json"):
		helper.config.setcreateconfig()
		logging.info("Config is copied")
	else:
		helper.config.resetcreateconfig()
		logging.error("Config is not copied")


def downloadtestdata():
	logging.info("Download Test Data")
	conf = helper.config.initconfig()
	try:
		cmd = "docker-compose -f ./freqtrade/docker-compose.yml run --rm freqtrade download-data --timeframes {} --days {}".format(conf['hyperopt_timeframe'],conf['hyperopt_timerange_days'])
		logging.info(cmd)
		os.system(cmd)
	except Exception as e:
		logging.error("Error while downloading data: " + str(e))
		print("Error while downloading data: " + str(e))
	logging.info("Finish Download Test Data")

def hyperopting(strategy, hyperopt_loss, hyperoptepochs, hyperopt_timeframe, hyperopt_timerange_days):
	logging.info("Hyperopt")
	now = datetime.datetime.now() - datetime.timedelta(days=1)
	before = datetime.datetime.now() - datetime.timedelta(days=int(hyperopt_timerange_days))
	timerange = before.strftime("%Y%m%d") + "-" + now.strftime("%Y%m%d")	

	# TODO Loop over all strategies
	# TODO Loop over all Hyperopt-loss
	# TODO Loop over all timeranges

	cmd = "docker-compose -f ./freqtrade/docker-compose.yml run --rm freqtrade hyperopt --hyperopt-loss {} --spaces buy sell roi stoploss trailing --strategy {} -j 4 -e {} --timeframe {} --timerange {}".format(hyperopt_loss,strategy,hyperoptepochs,hyperopt_timeframe,timerange)
	logging.info(cmd)
	os.system(cmd)
	logging.info("Finish Hyperopt")


    # Backtesting

	logging.info("Backtesting")

	try:
		cmd = "docker-compose -f ./freqtrade/docker-compose.yml run --rm freqtrade backtesting --strategy {} --timeframe {} --timerange {}".format(strategy,hyperopt_timeframe,timerange)
		logging.info(cmd)
		os.system(cmd)
	except Exception as e:
		logging.error("Error while backtesting: " + str(e))
		print("Error while backtesting: " + str(e))
	logging.info("Finish Backtesting")


	# Save in DB

	logging.info("Save in DB")
	try:
		with open('./freqtrade/user_data/backtest_results/.last_result.json', 'r') as f:
			data = json.load(f)
		with open('./freqtrade/user_data/backtest_results/{}'.format(data['latest_backtest']), 'r') as g:
			data2 = json.load(g)
		helper.sql.insert(data2, strategy, hyperopt_loss, hyperoptepochs, hyperopt_timeframe, hyperopt_timerange_days)
		data = data2['strategy_comparison'][0]
		helper.telegramsend.send(	"Backtesting finished for strategy {}\n" 
									"with hyperopt_loss {}\n" 
									"hyperoptepochs {}\n"
									"hyperopt_timeframe {}\n"
									"hyperopt_timerange_days {}\n"
									"Trades: {}\n"
									"Profit mean: {}\n"
									"Profit mean %: {}\n"
									"Profit sum: {}\n"
									"Profit sum %: {}\n"
									"Profit total abs: {}\n"
									"Profit total: {}\n"
									"Profit total %: {}\n"
									"Duration avg: {}\n"
									"Wins: {}\n"
									"Draws: {}\n"
									"Losses: {}\n"
									"Max drawdown account: {}\n"
									"Max drawdown abs: {}\n".format(strategy, hyperopt_loss, hyperoptepochs, hyperopt_timeframe, hyperopt_timerange_days, data['trades'], data['profit_mean'], data['profit_mean_pct'], data['profit_sum'], data['profit_sum_pct'], data['profit_total_abs'], data['profit_total'], data['profit_total_pct'], data['duration_avg'], data['wins'], data['draws'], data['losses'], data['max_drawdown_account'], data['max_drawdown_abs']))
	
	except Exception as e:
		logging.error("Error while saving in DB: " + str(e))
		print("Error while saving in DB: " + str(e))

		logging.info("Finish Save in DB")

def start_freqtrade():
	try:
		logging.info("Start Freqtrade")
		os.system("docker-compose -f ./freqtrade/docker-compose.yml up -d")
		logging.info("Finish Start Freqtrade")
	except Exception as e:
		logging.error("Error while starting Freqtrade: " + str(e))
		print("Error while starting Freqtrade: " + str(e))

def stop_freqtrade():
	try:
		logging.info("Stop Freqtrade")
		os.system("docker-compose -f ./freqtrade/docker-compose.yml down")
		logging.info("Finish Stop Freqtrade")
	except Exception as e:
		logging.error("Error while stopping Freqtrade: " + str(e))
		print("Error while stopping Freqtrade: " + str(e))
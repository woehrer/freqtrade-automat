import sqlite3
import time


def create():
    verbindung = sqlite3.connect("freqtrade/db.sqlite3")
    cursor = verbindung.cursor()

    # Backtesting
    cursor.execute('''CREATE TABLE IF NOT EXISTS backtesting
                    (key TEXT, trades INTEGER, profit_mean REAL, profit_mean_pct REAL, profit_sum REAL, profit_sum_pct REAL, profit_total_abs REAL, profit_total REAL, profit_total_pct REAL, duration_avg TEXT, wins INTEGER, draws INTEGER, losses INTEGER, max_drawdown_account REAL, max_drawdown_abs TEXT, strategy TEXT, hyperopt_loss TEXT, hyperoptepochs INTEGER, hyperopt_timeframe TEXT, hyperopt_timerange_days TEXT, timestamp TEXT)''')
    verbindung.commit()
    verbindung.close()

def insert(data, strategy, hyperopt_loss, hyperoptepochs, hyperopt_timeframe, hyperopt_timerange_days):
    data = data['strategy_comparison'][0]

    verbindung = sqlite3.connect("freqtrade/db.sqlite3")
    cursor = verbindung.cursor()
    execute = """INSERT INTO backtesting (key, trades, profit_mean, profit_mean_pct, profit_sum, profit_sum_pct, profit_total_abs, profit_total, profit_total_pct, duration_avg, wins, draws, losses, max_drawdown_account, max_drawdown_abs, strategy, hyperopt_loss, hyperoptepochs, hyperopt_timeframe, hyperopt_timerange_days, timestamp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""
    entities = (data['key'], data['trades'], data['profit_mean'], data['profit_mean_pct'], data['profit_sum'], data['profit_sum_pct'], data['profit_total_abs'], data['profit_total'], data['profit_total_pct'], data['duration_avg'], data['wins'], data['draws'], data['losses'], data['max_drawdown_account'], data['max_drawdown_abs'], strategy, hyperopt_loss, hyperoptepochs, hyperopt_timeframe, hyperopt_timerange_days,  int(time.time()))
    cursor.execute(execute,entities)
    verbindung.commit()
    verbindung.close()
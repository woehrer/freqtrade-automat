import configparser
import logging
import sys
import os

config = configparser.ConfigParser()
configDir = "./config"
configFile = "config/config.ini"

"""
Helper function to get the value of a config parameter

conf = helper.config.initconfig()
"""

def initconfig():
    if not os.path.isdir(configDir):
        os.mkdir(configDir)
        os.mkdir('./log')
    if not os.path.isfile(configFile):
        create()
    #Konfigdatei initialisieren
    try:
        #Config Datei auslesen
        config.read(configFile)
        return config['DEFAULT']
    except Exception as e:
        print("Error while loading the Config file:" + \
            str(sys.exc_info()) + "\n" + \
            str(e.message) + " " + str(e.args))
        logging.error("Error while loading the Config file" + str(sys.exc_info()) + \
            str(sys.exc_info()) + "\n" + \
            str(e.message) + " " + str(e.args))

def create():
    with open("config/config.template.ini", "r") as configtemp:
        config = configtemp.read()
    with open("config/config.ini", "w") as configfile:
        configfile.write(config)

def setdownloadedfreqtrade():
    config.set('DEFAULT','downloadedfreqtrade','True')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def resetdownloadedfreqtrade():
    config.set('DEFAULT','downloadedfreqtrade','False')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)


def setcreateduserdirectory():
    config.set('DEFAULT','createduserdirectory','True')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def resetcreateduserdirectory():
    config.set('DEFAULT','createduserdirectory','False')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)


def setdownloadedstrategies():
    config.set('DEFAULT','downloadedstrategies','True')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def resetdownloadedstrategies():
    config.set('DEFAULT','downloadedstrategies','False')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)


def setcreateconfig():
    config.set('DEFAULT','createconfig','True')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def resetcreateconfig():
    config.set('DEFAULT','createconfig','False')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)


def sethyperopting():
    config.set('DEFAULT','hyperopting','True')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def resethyperopting():
    config.set('DEFAULT','hyperopting','False')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def insertTelegramid(Telegramid):
    config.set('DEFAULT','Telegramid',Telegramid)
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setfreqtradebot():
    config.set('DEFAULT','freqtradebot','True')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def resetfreqtradebot():
    config.set('DEFAULT','freqtradebot','False')
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)